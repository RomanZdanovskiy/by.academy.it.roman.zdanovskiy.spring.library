package by.it.academy.bean;

import lombok.*;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Borrow {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToOne(fetch = FetchType.EAGER)
    private Book book;

    private long userId;

    public Borrow(Book book, long userId) {

        this.book = book;
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Borow{" +
                "id=" + id +
                ", book=" + book +
                ", user=" + userId +
                '}';
    }
}
