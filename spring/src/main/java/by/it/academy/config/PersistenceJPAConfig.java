package by.it.academy.config;

import org.h2.tools.Server;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "by.it.academy.repository")
@ComponentScan("by.it.academy.repository")
@PropertySource("classpath:database.properties")
public class PersistenceJPAConfig {

    @PostConstruct
    public void init() {
        try {
            Server.createTcpServer().start();
        } catch (SQLException e) {
            throw new RuntimeException("Fail start tcp h2 Server");
        }
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean em
                = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource());
        em.setPackagesToScan("by.it.academy.bean");

        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        em.setJpaProperties(getJpaProperties());

        return em;
    }

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(env.getProperty("hibernate.hikari.driverClassName"));
        dataSource.setUrl(env.getProperty("hibernate.hikari.jdbcUrl"));
        dataSource.setUsername(env.getProperty("hibernate.hikari.username"));
        dataSource.setPassword(env.getProperty("hibernate.hikari.password"));
        dataSource.setPassword(env.getProperty("hibernate.hikari.poolName"));
        dataSource.setPassword(env.getProperty("hibernate.hikari.connectionTimeout"));
        dataSource.setPassword(env.getProperty("hibernate.hikari.maxLifetime"));
        dataSource.setPassword(env.getProperty("hibernate.hikari.minimumIdle"));
        dataSource.setPassword(env.getProperty("hibernate.hikari.maximumPoolSize"));
        dataSource.setPassword(env.getProperty("hibernate.hikari.idleTimeout"));
        dataSource.setPassword(env.getProperty("hibernate.hikari.leakDetectionThreshold"));
        dataSource.setPassword(env.getProperty("hibernate.connection.provider_class"));
        return dataSource;
    }

    @Autowired
    Environment env;

    private Properties getJpaProperties() {
        Properties properties = new Properties();
        properties.setProperty("javax.persistence.schema-generation.database.action",
                env.getProperty("javax.persistence.schema-generation.database.action"));
        properties.setProperty("javax.persistence.sql-load-script-source",
                env.getProperty("javax.persistence.sql-load-script-source"));
        properties.setProperty("hibernate.dialect", env.getProperty("hibernate.dialect"));
        properties.setProperty("hibernate.use_sql_comments", env.getProperty("hibernate.use_sql_comments"));
        properties.setProperty("hibernate.show_sql", env.getProperty("hibernate.show_sql"));
        return properties;
    }

    @Bean
    public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory);
        return transactionManager;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }
}
