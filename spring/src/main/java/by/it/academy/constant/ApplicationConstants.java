package by.it.academy.constant;

public interface ApplicationConstants {

    String AUTHENTICATE_KEY = "authenticate";
    String REGISTRATE_KEY = "registration";
    String USER_KEY = "user";
    String USERS_KEY = "users";
    String ERROR_KEY = "error";
    String BOOKS_KEY = "books";
    String BORROWED_BOOKS_KEY = "borrowedbooks";
    String ALL_USERS_KEY = "allusers";

    String MAIN_JSP = "main";
    String ADMIN_JSP = "admin";
    String USER_JSP = "user";
}
