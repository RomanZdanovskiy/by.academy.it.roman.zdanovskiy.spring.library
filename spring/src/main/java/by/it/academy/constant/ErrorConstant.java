package by.it.academy.constant;

public interface ErrorConstant {

    String LOGIN_NOT_EXIST = "Current login does not exist";
    String LOGIN_EXIST = "Current login exist";
}
