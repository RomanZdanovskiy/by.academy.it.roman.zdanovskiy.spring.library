package by.it.academy.controller;

import by.it.academy.bean.*;
import by.it.academy.service.BookService;
import by.it.academy.service.BorrowService;
import by.it.academy.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

import java.sql.Date;
import java.time.LocalDate;

import static by.it.academy.constant.ApplicationConstants.*;
import static by.it.academy.constant.ErrorConstant.*;

@Controller
public class BorrowController {

    @Autowired
    private BorrowService borrowService;

    @Autowired
    private BookService bookService;

    @Autowired
    private UserService userService;

    @PostMapping("/borrow")
    public ModelAndView borrow(@RequestParam long userId, @RequestParam long bookId) {

        ModelAndView modelAndView;
        if (userService.getById(userId).getRole().equals(Role.ADMIN)) {

            modelAndView = new ModelAndView(ADMIN_JSP);
            modelAndView.addObject(ALL_USERS_KEY, userService.findAll());
        } else {

            modelAndView = new ModelAndView(USER_JSP);
        }
        bookService.borrowBook(bookId);
        Borrow borrow = new Borrow(bookService.findById(bookId),
                userId);
        borrowService.save(borrow);
        modelAndView.addObject(BOOKS_KEY, bookService.getAllBooks());
        modelAndView.addObject(BORROWED_BOOKS_KEY, borrowService.getBorowsByUser(userId));
        return modelAndView;
    }
}
