package by.it.academy.controller;

import by.it.academy.bean.Authenticate;
import by.it.academy.bean.Role;
import by.it.academy.bean.User;
import by.it.academy.service.AuthenticateService;
import by.it.academy.service.BookService;
import by.it.academy.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import static by.it.academy.constant.ApplicationConstants.*;
import static by.it.academy.constant.ErrorConstant.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
public class LoginController {

    @Autowired
    private AuthenticateService authenticateService;

    @Autowired
    private UserService userService;

    @Autowired
    private BookService bookService;

    @GetMapping("/")
    public ModelAndView mainPage(ModelMap modelMap) {

        ModelAndView modelAndView;

        if (!modelMap.containsAttribute(USER_KEY)) {

            modelAndView = new ModelAndView(MAIN_JSP);
            modelAndView.addObject(BOOKS_KEY, bookService.getAllBooks());

        } else {

            User user = (User) modelMap.getAttribute(USER_KEY);

            if (user.getRole().equals(Role.ADMIN)) {

                modelAndView = new ModelAndView(ADMIN_JSP);
                modelAndView.addObject(BOOKS_KEY, bookService.getAllBooks());
            } else {

                modelAndView = new ModelAndView(USER_JSP);
                modelAndView.addObject(BOOKS_KEY, bookService.getAllBooks());
            }
        }

        return modelAndView;

    }

    @GetMapping("/authenticate")
    public ModelAndView authenticate(ModelMap modelMap) {

        ModelAndView modelAndView = new ModelAndView(AUTHENTICATE_KEY);

        if (!modelMap.containsAttribute(AUTHENTICATE_KEY)) {

            Authenticate authenticate = new Authenticate();
            authenticate.setProfile_enable(true);
            modelAndView.addObject(AUTHENTICATE_KEY, authenticate);
        }

        if (modelMap.containsAttribute(ERROR_KEY)) {

            modelAndView.addObject(ERROR_KEY, modelMap.getAttribute(ERROR_KEY));
        }

        return modelAndView;
    }

    @PostMapping("/authenticate")
    public ModelAndView authenticate(@Valid @ModelAttribute Authenticate authenticate,
                                     BindingResult bindingResult, RedirectAttributes redirectAttributes,
                                     HttpSession httpSession) {
        try {
            ModelAndView modelAndView;

            if (bindingResult.hasErrors()) {

                modelAndView = new ModelAndView("redirect:/authenticate");
                redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.authenticate", bindingResult);
                redirectAttributes.addFlashAttribute(AUTHENTICATE_KEY, authenticate);

            } else {


                if (authenticateService.existsByLogin(authenticate.getLogin())) {

                    Authenticate auth = authenticateService.getByLoginAndPassword(authenticate.getLogin(),
                            authenticate.getPassword());
                    User user = auth.getUser();
                    if (user.getRole().equals(Role.ADMIN)) {

                        modelAndView = new ModelAndView(ADMIN_JSP);
                        httpSession.setAttribute(USER_KEY, user);
                        modelAndView.addObject(BOOKS_KEY, bookService.getAllBooks());
                        modelAndView.addObject(ALL_USERS_KEY, userService.findAll());

                    } else {

                        modelAndView = new ModelAndView(USER_JSP);
                        httpSession.setAttribute(USER_KEY, user);
                        modelAndView.addObject(BOOKS_KEY, bookService.getAllBooks());
                    }
                } else throw new RuntimeException(LOGIN_NOT_EXIST);
            }

            return modelAndView;

        } catch (RuntimeException e) {

            ModelAndView modelAndView = new ModelAndView(AUTHENTICATE_KEY);
            modelAndView.addObject(ERROR_KEY, e);
            return modelAndView;
        }
    }

    @GetMapping("/registrate")
    public ModelAndView registrate(ModelMap modelMap) {

        ModelAndView modelAndView = new ModelAndView(REGISTRATE_KEY);

        if(!modelMap.containsAttribute(AUTHENTICATE_KEY)) {

            Authenticate authenticate = new Authenticate();
            authenticate.setProfile_enable(true);
            User user = new User();
            user.setRole(Role.USER);
            modelAndView.addObject(AUTHENTICATE_KEY, authenticate);
            modelAndView.addObject(USER_KEY, user);
        }

        return modelAndView;
    }

    @PostMapping("/registrate")
    public ModelAndView registrate(@ModelAttribute User user, @Valid @ModelAttribute Authenticate authenticate,
                                   BindingResult bindingResult, RedirectAttributes redirectAttributes,
                                   HttpSession httpSession) {

        try {
            ModelAndView modelAndView;

            if (bindingResult.hasErrors()) {

                modelAndView = new ModelAndView("redirect:/registration");
                redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.authenticate", bindingResult);
                redirectAttributes.addFlashAttribute(AUTHENTICATE_KEY, authenticate);
//                redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.user", bindingResult);
//                redirectAttributes.addFlashAttribute(USER_KEY, user);

            } else {

                if (!authenticateService.existsByLogin(authenticate.getLogin())) {

                    modelAndView = new ModelAndView(USER_JSP);
                    user.setAuthenticate(authenticate);
                    authenticate.setUser(user);
                    user = authenticateService.save(authenticate).getUser();
                    httpSession.setAttribute(USER_KEY, user);
                    modelAndView.addObject(BOOKS_KEY, bookService.getAllBooks());
                } else throw new RuntimeException(LOGIN_EXIST);
            }

            return modelAndView;

        } catch (RuntimeException e) {

            ModelAndView modelAndView = new ModelAndView(REGISTRATE_KEY);
            modelAndView.addObject(ERROR_KEY, e);
            return modelAndView;
        }
    }

    @GetMapping("/logout")
    public ModelAndView logout(HttpSession httpSession) {

        httpSession.invalidate();
        ModelAndView modelAndView = new ModelAndView(MAIN_JSP);
        modelAndView.addObject(BOOKS_KEY, bookService.getAllBooks());
        return modelAndView;
    }
}
