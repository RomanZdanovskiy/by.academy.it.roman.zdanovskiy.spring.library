package by.it.academy.repository;

import by.it.academy.bean.Authenticate;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthenticateRepository extends CrudRepository<Authenticate, Long> {

    Authenticate getByLoginAndPassword(String login, String password);

    boolean existsByLogin(String login);

}
