package by.it.academy.repository;

import by.it.academy.bean.Borrow;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BorrowRepository extends CrudRepository<Borrow, Long> {

    public List<Borrow> getBorrowsByUserId(long id);

}
