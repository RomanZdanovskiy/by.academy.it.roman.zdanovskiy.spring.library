package by.it.academy.service;

import by.it.academy.bean.Authenticate;
import by.it.academy.repository.AuthenticateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class AuthenticateService {

    @Autowired
    private AuthenticateRepository authenticateRepository;

    public Authenticate save(Authenticate authenticate) {

        return authenticateRepository.save(authenticate);
    }

    public Authenticate getById(long id){

        return authenticateRepository.findById(id).orElse(null);
    }

    public Authenticate getByLoginAndPassword(String login, String password) {

        return authenticateRepository.getByLoginAndPassword(login, password);
    }

    public void deleteById(long id) {

        authenticateRepository.deleteById(id);
    }

    @Transactional(readOnly = true)
    public List<Authenticate> findAll() {

        return (List<Authenticate>) authenticateRepository.findAll();
    }

    public boolean existsByLogin(String login) {

        return authenticateRepository.existsByLogin(login);
    }


}
