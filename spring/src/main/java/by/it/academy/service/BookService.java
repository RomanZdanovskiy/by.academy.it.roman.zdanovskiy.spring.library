package by.it.academy.service;

import by.it.academy.bean.Book;
import by.it.academy.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class BookService {

    @Autowired
    private BookRepository bookRepository;

    public Book save(Book book){

        return bookRepository.save(book);
    }

    public Book findById(long id){

        return bookRepository.findById(id).orElse(null);
    }

    @Transactional(readOnly = true)
    public List<Book> getAllBooks(){

        return (List<Book>) bookRepository.findAll();
    }

    public void borrowBook(long id) {
        Book book = bookRepository.findById(id).orElse(null);
        int quantity = book.getQuantity() - 1;
        book.setQuantity(quantity);
        bookRepository.save(book);
    }
}
