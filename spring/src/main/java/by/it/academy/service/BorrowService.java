package by.it.academy.service;

import by.it.academy.bean.Book;
import by.it.academy.bean.Borrow;
import by.it.academy.repository.BorrowRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

@Service
@Transactional
public class BorrowService {

    @Autowired
    private BorrowRepository borrowRepository;

    public Borrow save(Borrow borrow) {

        return borrowRepository.save(borrow);
    }

    @Transactional(readOnly = true)
    public List<Borrow> getBorowsByUser(long id) {
        return (List<Borrow>) borrowRepository.getBorrowsByUserId(id);
    }

}
