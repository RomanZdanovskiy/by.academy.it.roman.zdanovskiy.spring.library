package by.it.academy.service;

import by.it.academy.bean.User;
import by.it.academy.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public List<User> findAll() {

        return (List<User>) userRepository.findAll();
    }

    public User save(User user) {

        return userRepository.save(user);
    }

    public User getById(long id) {

        return userRepository.findById(id).orElse(null);
    }

    public void deleteById(long id) {

        userRepository.deleteById(id);
    }


}
