insert into USER (NAME, SURNAME, EMAIL, AGE, ROLE) values ('Roman', 'Zdanovskiy', 'romario-mail@ya.ru',36, 'ADMIN');
insert into USER (NAME, SURNAME, EMAIL, AGE, ROLE) values ('Sergey', 'Zdanovskiy', 'sslayer@mail.ru',40, 'ADMIN');
insert into USER (NAME, SURNAME, EMAIL, AGE, ROLE) values ('Olga', 'Zdanovskaya', 'olga@ya.ru',35, 'USER');

insert into AUTHENTICATE (LOGIN, PASSWORD, PROFILE_ENABLE) values ('admin1', '1', true );
insert into AUTHENTICATE (LOGIN, PASSWORD, PROFILE_ENABLE) values ('admin2', '2', true );
insert into AUTHENTICATE (LOGIN, PASSWORD, PROFILE_ENABLE) values ('user1', '1', true );

insert into AUTHOR (NAME, SURNAME) VALUES ( 'Sergey', 'Lukiynenko' );
insert into AUTHOR (NAME, SURNAME) VALUES ( 'Gay Uliy', 'Orlovskiy' );
insert into AUTHOR (NAME, SURNAME) VALUES ( 'Bruce', 'Eckel' );

insert into GENRE (NAME) values ( 'fantasy' );
insert into GENRE (NAME) values ( 'scince' );

insert into BOOK (NAME, AUTHOR_ID, GENRE_ID, QUANTITY) values ( 'Nochnoy dozor', 1, 1, 10 );
insert into BOOK (NAME, AUTHOR_ID, GENRE_ID, QUANTITY) values ( 'Richard dlinie ruki', 2, 1, 5 );
insert into BOOK (NAME, AUTHOR_ID, GENRE_ID, QUANTITY) values ( 'Thinking in JAVA', 3, 2, 3 );
