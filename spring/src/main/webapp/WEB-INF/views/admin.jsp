<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <style>
        body {
            background: #fff;
            width: 100%;
            margin: 0;
        }

        .header_container {
            width: 100%;
            min-height: 40px;
            background-color: #3c2aca;
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        .footer_container {
            flex: 0 0 auto;
            min-height: 50px;
            width: 100%;
            background-color: #76CA41;
        }


        .header_title {
            color: rgb(248, 249, 250);
            margin: 0;
        }

        .body-container {
            position: relative;
        }

        .form_container {
            max-width: 300px;
            height: 100%;
            margin: 0;
        }

        .btn-message {
            border: 2px solid #76CA41;
            color: #76CA41;
            width: 100px;
        }

        .btn-message:hover {
            background-color: #76CA41;
            border: 2px solid #76CA41;
            color: #fff;
        }

        .btn-create {
            border: 2px solid #76CA41;
            color: #76CA41;
            width: 150px;
        }

        .btn-create:hover {
            background-color: #76CA41;
            border: 2px solid #76CA41;
            color: #fff;
        }

        .btn-press {
            background-color: #6BBC49;
            border: 2px solid #fff;
            color: #fff;
        }

        .btn-press:hover {
            background-color: #fff;
            border: 2px solid #6BBC49;
            color: #6BBC49;
        }

        .form-input {
            max-width: 300px;
        }

        .form-input:hover {
            border: 2px solid #76CA41;
        }

        .form-control:focus {
            border: 2px solid #76CA41;
            box-shadow: 0 0 0 0.01rem #76CA41;
        }

        .div-container {
            background-color: #F9F9F9;
            padding: 15px;
        }

        .table-title {
            margin-bottom: 15px;
        }

        .form_modified {
            margin: 0;
        }
    </style>
</head>
<body>
<div class="card-header header_container">
    <h3 class="header_title">Welcome ${sessionScope.user.name}</h3>

    <form class="form_container" action="<c:url value="/logout"/> " method="get">
        <input type="submit" class=" btn btn-light" name="action" value="logout">
    </form>
</div>
<h1>Добро пожаловать!</h1>

<h2 class="table-title"> Список книг нашей библиотеки</h2>
<table class="table table-bordered">
    <thead class="thead-light">
    <tr>
        <th>ID</th>
        <th>BOOK NAME</th>
        <th>AUTHOR</th>
        <th>GENRE</th>
        <th>QUANTITY</th>
        <th>BORROW</th>
    </tr>
    </thead>
    <c:forEach items="${books}" var="book">
        <tr>
            <td>${book.id} </td>
            <td>${book.name} </td>
            <td>${book.author.name} ${book.author.surName}</td>
            <td>${book.genre.name}</td>
            <td>${book.quantity}</td>
            <td>
                <form class="form_modified" action="<c:url value="/borrow"/> " method="post">
                    <input type="hidden" name="bookId" value="${book.id}">
                    <input type="hidden" name="userId" value="${sessionScope.user.id}">
                    <input type="submit" class=" btn btn-press" name="action" value="borrow">
                </form>
            </td>
        </tr>
    </c:forEach>
</table>

<h2 class="table-title"> Список книг в вашем пользовании</h2>
<table class="table table-bordered">
    <thead class="thead-light">
    <tr>
        <th>BORROW ID</th>
        <th>BOOK NAME</th>
        <th>BOOK ATHOR NAME</th>
        <th>BOOK ATHOR SURNAME</th>
        <th>BOOK GANRE</th>
        <th>USER ID</th>
    </tr>
    </thead>
    <c:forEach items="${borrowedbooks}" var="borrow">
        <tr>
            <td>${borrow.id} </td>
            <td>${borrow.book.name}</td>
            <td>${borrow.book.author.name}</td>
            <td>${borrow.book.author.surName}</td>
            <td>${borrow.book.genre.name} </td>
            <td>${borrow.userId} </td>
        </tr>
    </c:forEach>
</table>

<h2 class="table-title"> Список пользователей</h2>
<table class="table table-bordered">
    <thead class="thead-light">
    <tr>
        <th>USER ID</th>
        <th>USER NAME</th>
        <th>USER SURNAME</th>
        <th>USER EMAIL</th>
        <th>USER AGE</th>
        <th>USER ROLE</th>
        <th>USER LOGIN</th>
        <th>USER PASSWORD</th>
        <th>USER STATUS</th>
    </tr>
    </thead>
    <c:forEach items="${allusers}" var="user">
        <tr>
            <td>${user.id} </td>
            <td>${user.name}</td>
            <td>${user.surname}</td>
            <td>${user.email}</td>
            <td>${user.age} </td>
            <td>${user.role} </td>
            <td>${user.authenticate.login} </td>
            <td>${user.authenticate.password} </td>
            <td>${user.authenticate.profile_enable} </td>
        </tr>
    </c:forEach>
</table>



</body>
</html>
