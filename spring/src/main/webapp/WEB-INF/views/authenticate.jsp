<%@ page import="by.it.academy.constant.ApplicationConstants" %>
<%@ page import="org.springframework.web.servlet.ModelAndView" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Authenticate</title>
</head>
<body>

<h1>Заполните форму:</h1>

<form method="post" action="<c:url value="/authenticate" /> ">
    <p>Login</p>
    <p>
        <input type="text" name="login" placeholder="Please enter login"/>
              <form:errors path="authenticate.login"/>
    </p>
    <p>Password</p>
    <p>
        <input type="password" name="password" placeholder="Please enter password"/>
              <form:errors path="authenticate.password*"/>
    </p>
    <p><input type="hidden" name="profile_enable" value="${authenticate.profile_enable}"></p>
    <p><input type="submit" value="Authenticate"/></p>
</form>

</body>
</html>
