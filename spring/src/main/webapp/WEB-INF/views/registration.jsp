<%@ page import="by.it.academy.constant.ApplicationConstants" %>
<%@ page import="org.springframework.web.servlet.ModelAndView" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Registration</title>
</head>
<body>

<h1>Заполните форму:</h1>

<c:if test="${error != null}">
    <h3 style="color:red;">* ${error}</h3>
</c:if>

<form method="post" action="<c:url value="/registrate" /> ">
    <p>Login</p>
    <p>
        <input type="text" name="login" placeholder="Please enter login"/>
              <form:errors path="authenticate.login"/>
    </p>
    <p>Password</p>
    <p>
        <input type="password" name="password" placeholder="Please enter password"/>
              <form:errors path="authenticate.password*"/>
    </p>
    <p><input type="hidden" name="profile_enable" value="${authenticate.profile_enable}"></p>
    <p>Name</p>
    <p>
        <input type="text" name="name" placeholder="Please enter name"/>
<%--        <form:errors path="user.name"/>--%>
    </p>
    <p>Surname</p>
    <p>
        <input type="text" name="surname" placeholder="Please enter surname"/>
<%--        <form:errors path="user.surName"/>--%>
    </p>
    <p>Email</p>
    <p>
        <input type="text" name="email" placeholder="Please enter email"/>
<%--        <form:errors path="user.name"/>--%>
    </p>
    <p>Age</p>
    <p>
        <input type="number" name="age" placeholder="Please enter age"/>
<%--        <form:errors path="user.age"/>--%>
    </p>
    <p><input type="hidden" name="role" value="${user.role}"></p>
    <p><input type="submit" value="Registrate"/></p>
</form>

</body>
</html>
